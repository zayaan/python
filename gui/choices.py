import easygui

choice = easygui.buttonbox(
   "Select your favorite subject ? ",
   choices=["Chemistry", "Biology", "Physics", "Maths"])

if choice == "Chemistry":
   option = "You can be a scientist"
elif choice == "Biology":
   option = "You can be a doctor"
elif choice == "Physics":
   option = "You can be a physicist"
elif choice == "Maths":
   option = "You can be Computer scientist"

easygui.msgbox(option)

