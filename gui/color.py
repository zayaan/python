import easygui

color = easygui.choicebox(
    "Select your favourite color from this dropdown",
    choices=["Red", "Blue", "Green", "Purple", "Pink", "Yellow", "Other"])

easygui.msgbox("Now I know that your favourite color is " + color)
