import easygui

gender = easygui.buttonbox(
    "Please select your gender",
    choices=["Male","Female"])
age = easygui.integerbox("Please enter your age")

if gender == "Male":
    if age > 18:
        message = "You are a man"
    else:
        message = "You are a boy"
else:
    if age > 18:
        message = "You are a woman"
    else:
        message = "You are a girl"

easygui.msgbox(message)

