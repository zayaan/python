#
# This program will tell you your grade by looking at your
# marks.
#
import easygui

marks = float(easygui.enterbox("Enter your marks ? "))

if marks >= 80.0:
    grade = "A"
elif marks >= 70.0:
    grade = "B"
elif marks >= 60.0:
    grade = "C"
else:
    grade = "F"

easygui.msgbox("Your grade is " + grade)
